import { Message } from './message.type'

export function userMessage(text: string): Message {
  return {
    type: 'user',
    text,
  }
}

export function robotMessage(text: string): Message {
  return {
    type: 'robot',
    text,
  }
}
