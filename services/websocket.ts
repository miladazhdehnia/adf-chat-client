import { ref, onMounted, onUnmounted, Ref } from '@vue/composition-api'

export function useWebSocket(url: string) {
  const data: Ref<string> = ref('')
  const state: Ref<'OPEN' | 'CONNECTING' | 'CLOSING' | 'CLOSED'> =
    ref('CONNECTING')
  let ws: WebSocket
  const close: typeof ws.close = function (code, reason) {
    if (!ws) return

    ws.close(code, reason)
  }

  const send: typeof ws.send = function (data) {
    if (!ws) return

    ws.send(data)
  }
  const connect = function () {
    ws = new WebSocket(url)
    ws.onopen = () => {
      state.value = 'OPEN'
    }

    ws.onclose = ws.onerror = () => {
      state.value = 'CLOSED'
    }

    ws.onmessage = (e: MessageEvent) => {
      data.value = e.data
    }
  }
  const restart = function () {
    if (!ws) return

    ws.close()
    connect()
  }
  onMounted(() => {
    connect()
  })

  onUnmounted(() => {
    if (!ws) return

    ws.close()
  })

  return {
    data,
    state,
    close,
    send,
    connect,
    restart,
  }
}
